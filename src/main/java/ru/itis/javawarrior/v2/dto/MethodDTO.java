package ru.itis.javawarrior.v2.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ru.itis.javawarrior.v2.enums.MethodEnum;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
@Getter
@Setter
@AllArgsConstructor
public class MethodDTO {
    private MethodEnum methodEnum;

    private String methodString;

    private String description;

    public MethodDTO() {
    }
}
