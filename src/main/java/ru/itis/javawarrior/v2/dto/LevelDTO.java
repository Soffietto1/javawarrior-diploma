package ru.itis.javawarrior.v2.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */

@Builder
@Getter
@Setter
@AllArgsConstructor
public class LevelDTO {
    private Long levelid;
    private String levelName;
    private String creatorName;
    private String backgroundImage;
    private Double rating;
    private String description;
    private List<CellContentDTO> contents;
    private List<MethodDTO> methods;

    public LevelDTO() {
    }
}
