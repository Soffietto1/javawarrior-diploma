package ru.itis.javawarrior.v2.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import ru.itis.javawarrior.v2.enums.CellContentType;
import ru.itis.javawarrior.v2.enums.Size;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
@Getter
@Builder
@AllArgsConstructor
public class CellContentDTO {
    private Size size;
    private CellContentType type;
    private Integer position;
    private String modelImage;
    private Integer health;
    private Integer damage;

    public CellContentDTO() {
    }
}
