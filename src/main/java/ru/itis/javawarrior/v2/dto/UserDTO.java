package ru.itis.javawarrior.v2.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
public class UserDTO {
    private String email;
    private String password;

    public UserDTO() {
    }
}
