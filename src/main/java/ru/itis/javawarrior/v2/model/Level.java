package ru.itis.javawarrior.v2.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Сущность уровня
 *
 * @author Azat Khayrullin
 * @since 05.05.2019
 */
@Getter
@Setter
@Entity
public class Level extends AbstractBaseEntity {
    @ManyToOne
    private User creator;
    private String name;
    private Double rating;
    private String backgroundImage;
    private String description;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "level_id")
    private List<CellContent> contents;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Method> availableMethods;
}
