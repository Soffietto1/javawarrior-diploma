package ru.itis.javawarrior.v2.model;

import lombok.*;
import ru.itis.javawarrior.v2.enums.MethodEnum;

import javax.persistence.*;
import java.util.List;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
@Entity
@Data
@AllArgsConstructor
public class Method {

    @Id
    @Enumerated
    private MethodEnum methodEnum;

    private String methodString;

    private String description;

    public Method() {
    }
}
