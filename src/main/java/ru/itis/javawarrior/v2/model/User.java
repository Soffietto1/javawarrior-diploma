package ru.itis.javawarrior.v2.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

/**
 * Сущность пользователя
 *
 * @author Azat Khayrullin
 */
@Entity(name = "users")
@Setter
@Getter
public class User extends AbstractBaseEntity {

    private String name;
    private String email;
    private String password;
}
