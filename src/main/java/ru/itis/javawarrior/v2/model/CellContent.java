package ru.itis.javawarrior.v2.model;

import lombok.Getter;
import lombok.Setter;
import ru.itis.javawarrior.v2.enums.CellContentType;
import ru.itis.javawarrior.v2.enums.Size;

import javax.persistence.*;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
@Setter
@Getter
@Entity
public class CellContent extends AbstractBaseEntity {
    @Enumerated
    private Size size;
    @Enumerated
    private CellContentType type;
    private String modelImage;
    private Integer health;
    private Integer position;
    private Integer damage;

    @ManyToOne
    private Level level;

}
