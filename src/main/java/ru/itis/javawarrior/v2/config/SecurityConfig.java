package ru.itis.javawarrior.v2.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import ru.itis.javawarrior.v2.security.JWTAuthProvider;
import ru.itis.javawarrior.v2.security.JWTFilter;
import ru.itis.javawarrior.v2.service.UserService;

/**
 * Конфигурация для авторизации пользователя
 *
 * @author Azat Khayrullin
 */

@Configuration
@EnableWebSecurity
public class  SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserService userService;

    @Override
    @Order
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/**", "/swagger-ui.html").permitAll()
                .anyRequest().authenticated()
                .and()
                .antMatcher("/secure/**")
                .addFilterBefore(new JWTFilter(new JWTAuthProvider(userService)), BasicAuthenticationFilter.class);

    }

}

