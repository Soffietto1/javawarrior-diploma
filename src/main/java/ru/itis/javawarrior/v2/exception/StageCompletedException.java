package ru.itis.javawarrior.v2.exception;

/**
 * @author Damir Ilyasov
 */
public class StageCompletedException extends RuntimeException {
    public StageCompletedException() {
        super("You won!");
    }
}
