package ru.itis.javawarrior.v2.exception;

/**
 * @author Damir Ilyasov
 */
public class ValidateCodeException extends RuntimeException {

    public ValidateCodeException() {
    }

    public ValidateCodeException(String s) {
        super(s);
    }
}
