package ru.itis.javawarrior.v2.exception;

public class ServerException extends RuntimeException {
    public ServerException(String s) {
        super(s);
    }
}
