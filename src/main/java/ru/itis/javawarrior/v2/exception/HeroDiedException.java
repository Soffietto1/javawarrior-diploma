package ru.itis.javawarrior.v2.exception;

public class HeroDiedException extends RuntimeException {
    public HeroDiedException() {
        super("Hero died!");
    }
}
