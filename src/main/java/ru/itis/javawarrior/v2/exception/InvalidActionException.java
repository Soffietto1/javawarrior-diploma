package ru.itis.javawarrior.v2.exception;

/**
 * @author Damir Ilyasov
 */
public class InvalidActionException extends RuntimeException {
    public InvalidActionException() {
    }

    public InvalidActionException(String s) {
        super(s);
    }
}
