package ru.itis.javawarrior.v2.exception;

public class TimeOutException extends RuntimeException {
    public TimeOutException() {
        super("Timeout!");
    }
}
