package ru.itis.javawarrior.v2.service.impl;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.ReadOnlyJWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.itis.javawarrior.v2.model.User;
import ru.itis.javawarrior.v2.security.JWTToken;
import ru.itis.javawarrior.v2.service.SecurityService;
import ru.itis.javawarrior.v2.service.UserService;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
@Service
public class SecurityServiceImpl implements SecurityService {
    @Autowired
    private UserService userService;

    @Override
    public User getCurrentUser() {
        return userService.findByEmail(getCurrentUsersEmail());
    }

    @Override
    public String getCurrentUsersEmail() {
        JWTToken jwtToken = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
        ReadOnlyJWTClaimsSet claimsSet = jwtToken.getClaims();
        return (String) claimsSet.getClaim("email");
    }

    @Override
    public String generateToken(String email, String password) {
        JWTClaimsSet claimsSet = new JWTClaimsSet();
        claimsSet.setClaim("email", email);
        claimsSet.setClaim("password", password);
//        claimsSet.setSubject(email + encoder.encode(password));

        return this.signAndSerializeJWT(claimsSet, "jfodfusdhfiaushfiqwuehfqwuhfsdhflakhewqoifhkadlkahfiqwuehlchushfiuwhuw" +
                "asddddddddddddddddddddddd" +
                "asdasdasdasdasdasdasdadas" +
                "asdasdasdasdasdqwdqdqdqsd" +
                "qsdqsdqsdqsdqdqsdqsdqdqsd");
    }

    private String signAndSerializeJWT(JWTClaimsSet claimsSet, String secret) {
        JWSSigner signer = new MACSigner(secret);
        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS512), claimsSet);
        try {
            signedJWT.sign(signer);
            return signedJWT.serialize();
        } catch (JOSEException e) {
            e.printStackTrace();
            return null;
        }
    }

}
