package ru.itis.javawarrior.v2.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.itis.javawarrior.v2.exception.ServerException;
import ru.itis.javawarrior.v2.model.Method;
import ru.itis.javawarrior.v2.service.FileService;
import ru.itis.javawarrior.v2.service.LevelService;
import ru.itis.javawarrior.v2.service.ValidateService;
import ru.itis.javawarrior.v2.util.codevalidation.BannedConstructions;
import ru.itis.javawarrior.v2.util.codevalidation.Messages;
import ru.itis.javawarrior.v2.util.codevalidation.Validation;

import javax.tools.*;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ValidateServiceImpl implements ValidateService {

    @Autowired
    private final FileService fileService;
    private final List<Method> ALL_METHODS;

    @Autowired
    public ValidateServiceImpl(FileService fileService, LevelService levelService) {
        this.fileService = fileService;
        ALL_METHODS = levelService.getAllMethods();
    }

    @Override
    public Validation validate(String code, List<Method> availableMethods) {
        return checkMethodUsage(code, availableMethods);
    }

    private Validation checkMethodUsage(String code, List<Method> availableMethods) {
//        List<MethodEnum> availableMethodEnums = availableMethods.stream()
//                .map(Method::getMethodEnum)
//                .collect(Collectors.toList());
        for (Method method : ALL_METHODS) {
            if(code.contains(method.getMethodString()) && !availableMethods.contains(method)) {
                return new Validation(false, Messages.METHOD_USAGE_ERROR + method.getMethodString());
            }
        }
        return checkSyntax(code);
    }

    private Validation checkSyntax(String code) {

        try {
            fileService.writeToFile(code);
        } catch (IOException e) {
            throw new ServerException(Messages.VALIDATION_ERROR);
        }

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
        Iterable<? extends JavaFileObject> compilationUnits =
                fileManager.getJavaFileObjectsFromStrings(Collections.singletonList(FileService.TEST_FILE_PATH));

        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
        compiler.getTask(null, fileManager, diagnostics, null, null, compilationUnits).call();

        List<String> messages = new ArrayList<>();

        diagnostics.getDiagnostics().forEach(diagnostic -> {
            if (diagnostic.getLineNumber() != 4 && diagnostic.getPosition() != 55) {
                messages.add(diagnostic.getKind() + ": Line [" + (diagnostic.getLineNumber())
                        + "] Position [" + (diagnostic.getPosition())
                        + "] " + diagnostic.getMessage(Locale.ROOT) + "\n");
            }
        });

        if (messages.isEmpty()) {
            return checkSecurity(code);
        } else {
            return new Validation(false, Messages.INVALID_CODE + messages);
        }
    }

    private Validation checkSecurity(String code) {
        String regex;
        List<String> bannedConstructions = Arrays.stream(BannedConstructions.class.getFields())
                .map(this::getString)
                .collect(Collectors.toList());
        for (String bannedConstruction : bannedConstructions) {
            regex = ".*" + bannedConstruction + ".*";
            if (code.matches(regex)) {
                return new Validation(false, Messages.UNACCEPTABLE_CODE + " : " + bannedConstruction);
            }
        }
        return new Validation(true, Messages.VALIDATION_SUCCESS);
    }

    private String getString(Field field) {
        try {
            return (String) field.get(null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new ServerException(Messages.VALIDATION_ERROR);
        }
    }

    private int countOccurrencesOf(String code, Method method) {
        return StringUtils.countOccurrencesOf(code, method.getMethodString());
    }
}
