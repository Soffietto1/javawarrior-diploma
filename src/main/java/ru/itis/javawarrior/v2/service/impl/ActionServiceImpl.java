package ru.itis.javawarrior.v2.service.impl;

import ru.itis.javawarrior.v2.entity.Action;
import ru.itis.javawarrior.v2.enums.Animation;
import ru.itis.javawarrior.v2.enums.CellContentType;
import ru.itis.javawarrior.v2.enums.Size;
import ru.itis.javawarrior.v2.exception.HeroDiedException;
import ru.itis.javawarrior.v2.exception.StageCompletedException;
import ru.itis.javawarrior.v2.model.CellContent;
import ru.itis.javawarrior.v2.model.Level;
import ru.itis.javawarrior.v2.service.ActionService;

import java.util.*;

/**
 * Используется в компилированном классе
 */
public class ActionServiceImpl implements ActionService {

    private final Set<CellContentType> ENEMIES = new HashSet<>(Arrays.asList(
            CellContentType.RANGED_ENEMY,
            CellContentType.MELEE_ENEMY,
            CellContentType.SPIKE
    ));

    private List<Action> responseActions;
    private int position;
    private List<CellContent> stageCells;
    private CellContent hero;

    public ActionServiceImpl(Level level) {
        this.responseActions = new ArrayList<>();
        this.stageCells = level.getContents();
        this.hero = getHero();
        this.position = hero.getPosition();
    }

    @Override
    public void walk() {
        //stage completed if player at the last cell
        if (isLastCell()) {
            addAction(Animation.MOVE_FORWARD, 0);
            throw new StageCompletedException();
        }
        CellContent content = nextCell();
        if (content.getType() == CellContentType.EMPTY) {
            addAction(Animation.MOVE_FORWARD, 0);
            position++;
        } else {
            int damage = content.getDamage();
            addAction(Animation.MOVE_FORWARD_REJECTED, damage);
            damageHero(damage);
        }
    }

    @Override
    public void attack() {
        if (isLastCell()) {
            return;
        }
        CellContent content = nextCell();
        if (isEmptyAhead(content)) {
            addAction(Animation.SHOOT, 0);
        } else if (isEnemyAhead()) {
            int damage = content.getDamage();
            addAction(Animation.SHOOT, damage);
            damageHero(damage);
        }
        if (isEnemyAhead()) {
            stageCells.set(position + 1, new CellContent());
        }
    }

    @Override
    public void jump() {
        if (winWithJump()) {
            addAction(Animation.FLIP_FORWARD, 0);
            throw new StageCompletedException();
        } else {
            //cell after next cell is empty
            if (canJump()) {
                addAction(Animation.FLIP_FORWARD, 0);
                position += 2;
            } else if (isEnemyAhead()) {
                int damage = nextCell().getDamage();
                addAction(Animation.FLIP_FORWARD_REJECTED, damage);
                damageHero(damage);
            } else if(enemyAfterJump()) {
                int damage = afterJumpCell().getDamage();
                addAction(Animation.FLIP_FORWARD_REJECTED, damage);
                damageHero(damage);
            } else {
                addAction(Animation.FLIP_FORWARD_REJECTED, 0);
            }
        }
    }

    @Override
    public void rest() {
        addAction(Animation.REST, 0);
    }

    @Override
    public int health() {
        return hero.getHealth();
    }

    @Override
    public List<Action> getActions() {
        return responseActions;
    }

    @Override
    public boolean isEnemyAhead() {
        return !isLastCell() && ENEMIES.contains(nextCell().getType());
    }

    @Override
    public boolean isSpikeAhead() {
        return position + 2 <= stageCells.size() && nextCell().getType() == CellContentType.SPIKE;
    }

    private void damageHero(int damage) throws HeroDiedException {
        hero.setHealth(hero.getHealth() - damage);
        if (!isHeroAlive()) {
            addAction(Animation.DEATH, damage);
            throw new HeroDiedException();
        }
    }

    private boolean isHeroAlive() {
        return hero.getHealth() > 0;
    }

    private boolean isLastCell() {
        return position + 1 >= stageCells.size();
    }

    private boolean isEmptyAhead(CellContent content) {
        return content.getType() == CellContentType.EMPTY;
    }

    private CellContent nextCell() {
        return stageCells.get(position + 1);
    }

    private void addAction(Animation animation, int damaged) {
        Action action = new Action(hero.getHealth(), animation, damaged);
        responseActions.add(action);
    }

    private boolean winWithJump() {
        return position + 2 >= stageCells.size();
    }

    private boolean canJump() {
        return stageCells.get(position + 2).getType() == CellContentType.EMPTY
                && nextCell().getSize() == Size.SMALL;
    }

    private CellContent getHero() {
        for (CellContent stageCell : stageCells) {
            if (stageCell.getType() == CellContentType.HERO)
                    return stageCell;
        }
        return new CellContent();
    }

    private boolean enemyAfterJump() {
        return ENEMIES.contains(afterJumpCell().getType());
    }

    private CellContent afterJumpCell() {
        return stageCells.get(position + 2);
    }
}

