package ru.itis.javawarrior.v2.service;

import ru.itis.javawarrior.v2.model.User;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public interface UserService {

    void save(User user);

    User findByEmail(String email);
}
