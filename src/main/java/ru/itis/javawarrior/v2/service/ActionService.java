package ru.itis.javawarrior.v2.service;

import ru.itis.javawarrior.v2.entity.Action;

import java.util.List;

/**
 * Сервис доступных клиенту действий
 *
 * @author Azat Khayrullin
 */
public interface ActionService extends Actions {
    List<Action> getActions();
}
