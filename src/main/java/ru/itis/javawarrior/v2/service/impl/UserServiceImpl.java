package ru.itis.javawarrior.v2.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.javawarrior.v2.dao.UserRepository;
import ru.itis.javawarrior.v2.model.User;
import ru.itis.javawarrior.v2.service.UserService;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Override
    public void save(User user) {
        repository.save(user);
    }

    @Override
    public User findByEmail(String email) {
        return repository.findOneByEmail(email.toLowerCase());
    }
}
