package ru.itis.javawarrior.v2.service.impl;

import net.openhft.compiler.CompilerUtils;
import org.springframework.stereotype.Service;
import ru.itis.javawarrior.v2.entity.GameResult;
import ru.itis.javawarrior.v2.model.Level;
import ru.itis.javawarrior.v2.service.CompileService;
import ru.itis.javawarrior.v2.util.compile.CodeExecutor;
import ru.itis.javawarrior.v2.util.compile.JavaCodeParts;

/**
 * Сервис компиляции кода написанного пользователем
 *
 * @author Azat Khayrullin
 */
@Service
public class CompileServiceImpl implements CompileService {

    private int operationNumber = 0;

    @Override
    public GameResult compile(String code, Level level) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
                GameResult response = null;
        CodeExecutor codeExecutor;
        String className = "ru.itis.javawarrior.v2.util.compile." + getClassName();
        String classCode = (JavaCodeParts.BEGINNING_OF_CODE_1_PART + code).replace("TestFile", getClassName());
        Class aClass = CompilerUtils.CACHED_COMPILER.loadFromJava(className, classCode);
        if (aClass != null) {
            codeExecutor = (CodeExecutor) aClass.newInstance();
            if (codeExecutor != null) {
                response = codeExecutor.main(level);
            }
        }
        operationNumber++;
        return response;
    }

    private String getClassName() {
        return String.format("CompiledClass%s", operationNumber);
    }
}
