package ru.itis.javawarrior.v2.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.javawarrior.v2.dao.CellContentRepository;
import ru.itis.javawarrior.v2.model.CellContent;
import ru.itis.javawarrior.v2.service.CellContentService;

import java.util.List;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
@Service
public class CellContentServiceImpl implements CellContentService {

    @Autowired
    private CellContentRepository repository;

    @Override
    public void save(CellContent cellContent) {
        repository.save(cellContent);
    }

    @Override
    public void save(List<CellContent> cellContents) {
        repository.saveAll(cellContents);
    }
}
