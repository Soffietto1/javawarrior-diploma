package ru.itis.javawarrior.v2.service;

import ru.itis.javawarrior.v2.entity.GameResult;
import ru.itis.javawarrior.v2.model.Level;

/**
 * Сервис компиляции кода
 *
 * @author Azat Khayrullin
 */
public interface CompileService {
    GameResult compile(String code, Level level) throws ClassNotFoundException, IllegalAccessException, InstantiationException;
}
