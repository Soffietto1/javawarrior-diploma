package ru.itis.javawarrior.v2.service;

import ru.itis.javawarrior.v2.model.User;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public interface SecurityService {
    User getCurrentUser();

    String getCurrentUsersEmail();

    String generateToken(String email, String password);
}
