package ru.itis.javawarrior.v2.service;

import ru.itis.javawarrior.v2.model.CellContent;

import java.util.List;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public interface CellContentService {
    void save(CellContent cellContent);
    void save(List<CellContent> cellContents);
}
