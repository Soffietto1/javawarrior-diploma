package ru.itis.javawarrior.v2.service;

import ru.itis.javawarrior.v2.model.Method;
import ru.itis.javawarrior.v2.util.codevalidation.Validation;

import java.util.List;

/**
 * Интерфейс сервиса валидации кода
 *
 * @author Azat Khayrullin
 */
public interface ValidateService {
    Validation validate(String code, List<Method> availableMethods);

}
