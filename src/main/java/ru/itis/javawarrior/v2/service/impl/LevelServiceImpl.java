package ru.itis.javawarrior.v2.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.javawarrior.v2.dao.LevelRepository;
import ru.itis.javawarrior.v2.dao.MethodRepository;
import ru.itis.javawarrior.v2.enums.MethodEnum;
import ru.itis.javawarrior.v2.model.Level;
import ru.itis.javawarrior.v2.model.Method;
import ru.itis.javawarrior.v2.service.LevelService;

import java.util.List;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
@Service
public class LevelServiceImpl implements LevelService {

    @Autowired
    private LevelRepository repository;

    @Autowired
    private MethodRepository methodRepository;

    @Override
    public void save(Level level) {
        repository.save(level);
    }

    @Override
    public Level findById(Long levelId) {
        return repository.findById(levelId).get();
    }

    @Override
    public List<Level> getAll() {
        return repository.findAll();
    }

    @Override
    public List<Level> findAllByCreatorId(Long creatorId) {
        return repository.findAllByCreatorId(creatorId);
    }

    @Override
    public List<Method> findAllMethods(List<MethodEnum> methodEnums) {
        return methodRepository.findAllById(methodEnums);
    }

    @Override
    public List<Method> getAllMethods() {
        return methodRepository.findAll();
    }
}
