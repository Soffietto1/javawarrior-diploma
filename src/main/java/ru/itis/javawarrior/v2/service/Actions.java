package ru.itis.javawarrior.v2.service;

/**
 * Интерфейс доступных действтий
 *
 * @author Azat Khayrullin
 */
public interface Actions {
    void walk();

    void attack();

    void jump();

    void rest();

    int health();

    boolean isEnemyAhead();

    boolean isSpikeAhead();
}
