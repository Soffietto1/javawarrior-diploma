package ru.itis.javawarrior.v2.service;

import java.io.IOException;

public interface FileService {
    String TEST_FILE_PATH = "TestFile.java";
    void writeToFile(String code) throws IOException;
}
