package ru.itis.javawarrior.v2.service;

import ru.itis.javawarrior.v2.enums.MethodEnum;
import ru.itis.javawarrior.v2.model.Level;
import ru.itis.javawarrior.v2.model.Method;

import java.util.List;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public interface LevelService {

    void save(Level level);

    Level findById(Long levelId);

    List<Level> getAll();

    List<Level> findAllByCreatorId(Long creatorId);

    List<Method> findAllMethods(List<MethodEnum> methodEnums);

    List<Method> getAllMethods();
}
