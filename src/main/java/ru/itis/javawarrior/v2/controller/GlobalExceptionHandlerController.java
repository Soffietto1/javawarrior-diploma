package ru.itis.javawarrior.v2.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.itis.javawarrior.v2.entity.GameResult;
import ru.itis.javawarrior.v2.exception.ValidateCodeException;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * Контроллер перехвата ошибок выполнения, не связанных с реализацией
 *
 * @author Azat Khayrullin
 */
@ControllerAdvice
public class GlobalExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<GameResult> exception(Exception e) {
        return new ResponseEntity<>(GameResult.error(e.getMessage()), INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ValidateCodeException.class)
    public ResponseEntity<GameResult> validateCodeException(ValidateCodeException e) {
        return ResponseEntity.ok(GameResult.error(e.getMessage()));
    }
}
