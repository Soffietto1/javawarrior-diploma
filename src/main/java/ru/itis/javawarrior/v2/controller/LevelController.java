package ru.itis.javawarrior.v2.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.javawarrior.v2.dto.LevelDTO;
import ru.itis.javawarrior.v2.dto.MethodDTO;
import ru.itis.javawarrior.v2.json.LevelJson;
import ru.itis.javawarrior.v2.model.Level;
import ru.itis.javawarrior.v2.model.Method;
import ru.itis.javawarrior.v2.model.User;
import ru.itis.javawarrior.v2.service.LevelService;
import ru.itis.javawarrior.v2.service.SecurityService;
import ru.itis.javawarrior.v2.util.LevelUtil;
import ru.itis.javawarrior.v2.util.converters.LevelConverter;
import ru.itis.javawarrior.v2.util.converters.MethodConverter;

import java.util.List;

/**
 * Контроллер редактора уровнә
 *
 * @author Azat Khayrullin
 * @since 05.05.2019
 */
@RestController
@RequestMapping("/secure")
public class LevelController {

    @Autowired
    private LevelService levelService;
    @Autowired
    private SecurityService securityService;

    @ApiOperation("Editor")
    @CrossOrigin
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @PostMapping("/editor")
    public ResponseEntity<String> openLevelEditor(@RequestBody LevelJson levelJson) {
        User currentUser = securityService.getCurrentUser();
        if(currentUser == null) {
            return new ResponseEntity<>("Not logged in", HttpStatus.BAD_REQUEST);
        }
        Level level = LevelUtil.newLevel(levelJson);
        level.setCreator(currentUser);
        List<Method> methods = levelService.findAllMethods(levelJson.getMethods());
        if(methods.isEmpty()) {
            methods = levelService.getAllMethods();
        }
        level.setAvailableMethods(methods);
        levelService.save(level);
        return ResponseEntity.ok("Successfully saved level with id: " + level.getId());
    }

    @ApiOperation("Get level list created by current user")
    @CrossOrigin
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @GetMapping("/levels/current_user")
    public ResponseEntity<List<LevelDTO>> getLevelsByUser() {
        User currentUser = securityService.getCurrentUser();
        if(currentUser == null) {
            return new ResponseEntity("Not logged in", HttpStatus.BAD_REQUEST);
        }
        List<Level> levels = levelService.findAllByCreatorId(currentUser.getId());
        List<LevelDTO> levelDTOS = LevelConverter.toDtoList(levels);
        return ResponseEntity.ok(levelDTOS);
    }

    @ApiOperation("Get level list by current user")
    @CrossOrigin
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @GetMapping("/level")
    public ResponseEntity<LevelDTO> getLevelById(@RequestParam(name = "level_id") Long levelId) {
        Level level = levelService.findById(levelId);
        LevelDTO levelDTO = LevelConverter.toDTO(level);
        return ResponseEntity.ok(levelDTO);
    }

    @ApiOperation("Get all levels list")
    @CrossOrigin
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @GetMapping("/levels")
    public ResponseEntity<List<LevelDTO>> getAllLevels() {
        List<Level> levels = levelService.getAll();
        List<LevelDTO> levelDTOS = LevelConverter.toDtoList(levels);
        return ResponseEntity.ok(levelDTOS);
    }

    @ApiOperation("Get all methods list")
    @CrossOrigin
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @GetMapping("/methods")
    public ResponseEntity<List<MethodDTO>> getAllMethods() {
        List<Method> methods = levelService.getAllMethods();
        List<MethodDTO> methodDTOs = MethodConverter.toDto(methods);
        return ResponseEntity.ok(methodDTOs);
    }

}
