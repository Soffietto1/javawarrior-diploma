package ru.itis.javawarrior.v2.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.javawarrior.v2.entity.GameResult;
import ru.itis.javawarrior.v2.exception.ValidateCodeException;
import ru.itis.javawarrior.v2.json.CompileJson;
import ru.itis.javawarrior.v2.model.Level;
import ru.itis.javawarrior.v2.service.CompileService;
import ru.itis.javawarrior.v2.service.LevelService;
import ru.itis.javawarrior.v2.service.ValidateService;
import ru.itis.javawarrior.v2.util.codevalidation.Validation;


/**
 * Контроллер перехвата ошибок выполнения, не связанных с реализацией
 *
 * @author Azat Khayrullin
 */
@RestController
@RequestMapping("/secure")
public class CompileController {

    private final Logger LOGGER = LoggerFactory.getLogger(CompileController.class);

    @Autowired
    private CompileService compileService;
    @Autowired
    private LevelService levelService;
    @Autowired
    private ValidateService validateService;

    @CrossOrigin
    @ApiOperation("Available actions:\n" +
            "    walk();\n" +
            "    attack();\n" +
            "    jump();\n" +
            "    rest();\n" +
            "    health();\n" +
            "    enemyAhead();\n" +
            "    spikesAhead();"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")
    })
    @PostMapping("/compile")
    public ResponseEntity<GameResult> compile(@RequestBody CompileJson compileJson) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        Level level = levelService.findById(compileJson.getLevelId());
        Validation validation = validateService.validate(compileJson.getInputtedCode(), level.getAvailableMethods());
        LOGGER.info("Compiling");
        if (!validation.isValid()) {
            LOGGER.info("Code is not valid");
            throw new ValidateCodeException(validation.getMessage());
        }
        LOGGER.info("Code is valid");
        GameResult result = compileService.compile(compileJson.getInputtedCode(), level);
        LOGGER.info("Result is ok");
        return ResponseEntity.ok(result);
    }
}
