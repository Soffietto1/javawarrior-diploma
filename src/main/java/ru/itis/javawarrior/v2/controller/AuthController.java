package ru.itis.javawarrior.v2.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.javawarrior.v2.dto.UserDTO;
import ru.itis.javawarrior.v2.model.User;
import ru.itis.javawarrior.v2.service.SecurityService;
import ru.itis.javawarrior.v2.service.UserService;
import ru.itis.javawarrior.v2.util.converters.UserConverter;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
@RestController
@CrossOrigin
public class AuthController {

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    @Autowired
    private UserService userService;
    @Autowired
    private SecurityService securityService;

    @ApiOperation("Sign up")
    @CrossOrigin
    @PostMapping("/registration")
    public ResponseEntity<String> signUp(@RequestBody UserDTO userDto) {
        User user = userService.findByEmail(userDto.getEmail());
        if (user != null) {
            return new ResponseEntity<>("Already exists", HttpStatus.BAD_REQUEST);
        }
        user = UserConverter.toEntity(userDto);
        userService.save(user);
        String token = securityService.generateToken(userDto.getEmail(), userDto.getPassword());
        return ResponseEntity.ok(token);
    }


    @ApiOperation("Sign in")
    @CrossOrigin
    @PostMapping("/login")
    public ResponseEntity<String> loginAndGetToken(@RequestBody UserDTO userDTO) {
        String email = userDTO.getEmail();
        String password = userDTO.getPassword();
        User user = userService.findByEmail(email);
        if (user == null) {
            return new ResponseEntity<>("Wrong email", HttpStatus.BAD_REQUEST);
        }
        if (!ENCODER.matches(password, user.getPassword())) {
            return new ResponseEntity<>("Wrong password", HttpStatus.BAD_REQUEST);
        }
        String token = securityService.generateToken(email, password);
        return ResponseEntity.ok(token);
    }

}
