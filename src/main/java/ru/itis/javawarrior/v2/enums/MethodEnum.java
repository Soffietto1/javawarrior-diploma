package ru.itis.javawarrior.v2.enums;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public enum  MethodEnum {
    WALK,
    JUMP,
    REST,
    ATTACK,
    HEALTH,
    IS_ENEMY_AHEAD
}
