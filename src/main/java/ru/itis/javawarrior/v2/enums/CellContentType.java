package ru.itis.javawarrior.v2.enums;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public enum CellContentType {
    HERO,
    MELEE_ENEMY,
    RANGED_ENEMY,
    SPIKE,
    EMPTY
}
