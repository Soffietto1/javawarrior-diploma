package ru.itis.javawarrior.v2.enums;

/**
 * Размер содержимого клетки
 *
 * @author Azat Khayrullin
 */

public enum Size {
    TALL, SMALL
}
