package ru.itis.javawarrior.v2.enums;

/**
 * @author Damir Ilyasov
 */

public enum Animation {
    MOVE_FORWARD, MOVE_FORWARD_REJECTED,
    FLIP_FORWARD, FLIP_FORWARD_REJECTED,
    SHOOT,
    DEATH,
    REST
}
