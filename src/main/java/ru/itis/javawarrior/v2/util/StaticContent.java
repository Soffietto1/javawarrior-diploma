package ru.itis.javawarrior.v2.util;

import ru.itis.javawarrior.v2.enums.CellContentType;
import ru.itis.javawarrior.v2.enums.Size;
import ru.itis.javawarrior.v2.model.CellContent;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public class StaticContent {

    public static CellContent hero() {
        CellContent hero = new CellContent();
        hero.setHealth(100);
        hero.setModelImage("0");
        hero.setDamage(20);
        hero.setType(CellContentType.HERO);
        hero.setSize(Size.TALL);
        return hero;
    }

    public static CellContent emptyCell() {
        CellContent emptyCell = new CellContent();
        emptyCell.setType(CellContentType.EMPTY);
        emptyCell.setSize(Size.SMALL);
        return emptyCell;
    }

    public static CellContent getSpike() {
        CellContent spike = new CellContent();
        spike.setType(CellContentType.SPIKE);
        spike.setDamage(20);
        spike.setSize(Size.SMALL);
        return spike;
    }

    public static CellContent meleeEnemy() {
        CellContent enemy = new CellContent();
        enemy.setSize(Size.TALL);
        enemy.setModelImage("0");
        enemy.setType(CellContentType.MELEE_ENEMY);
        enemy.setHealth(50);
        enemy.setDamage(20);
        return enemy;
    }
}
