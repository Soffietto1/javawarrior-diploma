package ru.itis.javawarrior.v2.util.codevalidation;

public class Messages {
    //Exceptions
    public static final String VALIDATION_ERROR= "Серверная ошибка валидации";
    public static final String UNACCEPTABLE_CODE = "Вы используете запрещенные конструкции";
    public static final String INVALID_CODE = "Код не компилируется, допущена ошибка : \n";
    public static final String METHOD_COUNT_USAGE_ERROR = "Нельзя использовать каждый метод больше одного раза";
    public static final String METHOD_USAGE_ERROR = "Нельзя использовать метод: ";

    //Successes
    public static final String VALIDATION_SUCCESS = "Валидация успешна";
}
