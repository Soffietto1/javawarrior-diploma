package ru.itis.javawarrior.v2.util.converters;

import ru.itis.javawarrior.v2.dto.MethodDTO;
import ru.itis.javawarrior.v2.model.Method;

import java.util.List;
import java.util.stream.Collectors;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public class MethodConverter {

    public static List<MethodDTO> toDto(List<Method> methods) {
        return methods.stream()
                .map(MethodConverter::toDto)
                .collect(Collectors.toList());
    }

    public static MethodDTO toDto(Method method) {
        MethodDTO dto = new MethodDTO();
        dto.setMethodString(method.getMethodString());
        dto.setMethodEnum(method.getMethodEnum());
        dto.setDescription(method.getDescription());
        return dto;
    }

}
