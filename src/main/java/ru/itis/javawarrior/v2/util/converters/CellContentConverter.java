package ru.itis.javawarrior.v2.util.converters;

import ru.itis.javawarrior.v2.dto.CellContentDTO;
import ru.itis.javawarrior.v2.model.CellContent;

import java.util.List;
import java.util.stream.Collectors;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public class CellContentConverter {

    public static List<CellContentDTO> toDtoList(List<CellContent> contents) {
        return contents.stream()
                .map(CellContentConverter::toDTO)
                .collect(Collectors.toList());
    }

    public static CellContentDTO toDTO(CellContent content) {
        return CellContentDTO.builder()
                .damage(content.getDamage())
                .health(content.getHealth())
                .modelImage(content.getModelImage())
                .position(content.getPosition())
                .size(content.getSize())
                .type(content.getType())
                .build();
    }

    public static List<CellContent> toEntityList(List<CellContentDTO> dtos) {
        return dtos.stream()
                .map(CellContentConverter::toEntity)
                .collect(Collectors.toList());
    }

    public static CellContent toEntity(CellContentDTO dto) {
        CellContent cellContent = new CellContent();
        cellContent.setPosition(dto.getPosition());
        cellContent.setHealth(dto.getHealth());
        cellContent.setDamage(dto.getDamage());
        cellContent.setType(dto.getType());
        cellContent.setSize(dto.getSize());
        cellContent.setModelImage(dto.getModelImage());
        return cellContent;
    }
}
