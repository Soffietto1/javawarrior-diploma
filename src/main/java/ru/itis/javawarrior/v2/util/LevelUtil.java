package ru.itis.javawarrior.v2.util;

import ru.itis.javawarrior.v2.json.LevelJson;
import ru.itis.javawarrior.v2.model.CellContent;
import ru.itis.javawarrior.v2.model.Level;
import ru.itis.javawarrior.v2.util.converters.CellContentConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Статические уровни
 *
 * @author Azat Khayrullin
 */
public class LevelUtil {
    private static final List<CellContent> CELLS = new ArrayList<>();

    public static List<CellContent> firstLevel() {
        CELLS.clear();
        addCells(
                StaticContent.hero(),
                StaticContent.emptyCell(),
                StaticContent.emptyCell(),
                StaticContent.emptyCell(),
                StaticContent.emptyCell()
        );
        return CELLS;
    }

    public static List<CellContent> secondLevel() {
        CELLS.clear();
        addCells(
                StaticContent.hero(),
                StaticContent.emptyCell(),
                StaticContent.emptyCell(),
                StaticContent.getSpike(),
                StaticContent.emptyCell()
        );
        return CELLS;
    }

    public static List<CellContent> thirdLevel() {
        CELLS.clear();
        addCells(
                StaticContent.hero(),
                StaticContent.getSpike(),
                StaticContent.emptyCell(),
                StaticContent.emptyCell(),
                StaticContent.meleeEnemy()
        );
        return CELLS;
    }

    private static void addCells(CellContent ... cellContents) {
        for(CellContent cellContent : cellContents) {
            cellContent.setPosition(CELLS.size());
            CELLS.add(cellContent);
        }
    }

    public static Level newLevel(List<CellContent> contents) {
        Level level = new Level();
        level.setRating(0d);
        level.setContents(contents);
        return level;
    }

    public static Level newLevel(LevelJson json) {
        Level level = new Level();
        level.setBackgroundImage(json.getBackgroundImage());
        List<CellContent> contents = CellContentConverter.toEntityList(json.getContents());
        level.setRating(0d);
        level.setContents(contents);
        return level;
    }
}
