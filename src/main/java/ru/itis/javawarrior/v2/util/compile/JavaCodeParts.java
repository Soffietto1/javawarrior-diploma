package ru.itis.javawarrior.v2.util.compile;

/**
 * Части кода
 *
 * @author Azat Khayrullin
 */
public abstract class JavaCodeParts {
    public static final String BEGINNING_OF_CODE_1_PART = "package ru.itis.javawarrior.v2.util.compile;\n" +
            "\n";
}
