package ru.itis.javawarrior.v2.util.converters;

import ru.itis.javawarrior.v2.dto.LevelDTO;
import ru.itis.javawarrior.v2.model.Level;

import java.util.List;
import java.util.stream.Collectors;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public class LevelConverter {

    public static List<LevelDTO> toDtoList(List<Level> levels) {
        return levels.stream()
                .map(LevelConverter::toDTO)
                .collect(Collectors.toList());
    }

    public static LevelDTO toDTO(Level level) {
        return LevelDTO.builder()
                .levelName(level.getName())
                .levelid(level.getId())
                .backgroundImage(level.getBackgroundImage())
                .description(level.getDescription())
                .creatorName(level.getCreator().getName())
                .rating(level.getRating())
                .methods(MethodConverter.toDto(level.getAvailableMethods()))
                .contents(CellContentConverter.toDtoList(level.getContents()))
                .build();
    }
}
