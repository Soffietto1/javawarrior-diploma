package ru.itis.javawarrior.v2.util.compile;

import ru.itis.javawarrior.v2.entity.GameResult;
import ru.itis.javawarrior.v2.model.Level;

/**
 * Интерфейс запуска кода
 *
 * @author Azat Khayrullin
 */
public interface CodeExecutor {
    GameResult main(Level level);
}
