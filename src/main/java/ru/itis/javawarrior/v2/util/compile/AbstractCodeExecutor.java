package ru.itis.javawarrior.v2.util.compile;

import ru.itis.javawarrior.v2.entity.GameResult;
import ru.itis.javawarrior.v2.exception.HeroDiedException;
import ru.itis.javawarrior.v2.exception.StageCompletedException;
import ru.itis.javawarrior.v2.exception.TimeOutException;
import ru.itis.javawarrior.v2.model.Level;
import ru.itis.javawarrior.v2.service.ActionService;
import ru.itis.javawarrior.v2.service.Actions;
import ru.itis.javawarrior.v2.service.impl.ActionServiceImpl;

/**
 * Класс для хранения методов, которые должен вызывать компилированный класс,
 * Также инициализирует необходимые переменные
 *
 * @author Azat Khayrullin
 */
public abstract class AbstractCodeExecutor implements CodeExecutor, Actions {

    private static final int AVAILABLE_ITERATIONS_NUMBER = 20;
    private boolean isActionMade;

    protected ActionService actionService;

    @Override
    public GameResult main(Level level) {
        actionService = new ActionServiceImpl(level);
        try {
            int count = 0;
            while (count != AVAILABLE_ITERATIONS_NUMBER) {
                isActionMade = false;
                start();
                count++;
            }
            //тип если герой не умер и не выиграл
            throw new TimeOutException();
        } catch (StageCompletedException e) {
            return GameResult.completed(e.getMessage(), actionService.getActions());
        } catch (HeroDiedException | TimeOutException e) {
            return GameResult.notCompleted(e.getMessage(), actionService.getActions());
        } catch (Exception ex) {
            return GameResult.error(ex.getMessage());
        }
    }

    public abstract void start();

    @Override
    public void walk() {
        if (!isActionMade) {
            actionService.walk();
            isActionMade = true;
        }
    }

    @Override
    public void attack() {
        if (!isActionMade) {
            actionService.attack();
            isActionMade = true;
        }
    }

    @Override
    public void jump() {
        if (!isActionMade) {
            actionService.jump();
            isActionMade = true;
        }
    }

    @Override
    public void rest() {
        if (!isActionMade) {
            actionService.rest();
            isActionMade = true;
        }
    }

    @Override
    public int health() {
        return actionService.health();
    }

    @Override
    public boolean isEnemyAhead() {
        return actionService.isEnemyAhead();
    }

    @Override
    public boolean isSpikeAhead() {
        return actionService.isSpikeAhead();
    }
}
