package ru.itis.javawarrior.v2.util.converters;

//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itis.javawarrior.v2.dto.UserDTO;
import ru.itis.javawarrior.v2.model.User;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public class UserConverter {

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    public static User toEntity(UserDTO userDTO) {
        User user = new User();
        user.setEmail(userDTO.getEmail().toLowerCase());
        user.setPassword(ENCODER.encode(userDTO.getPassword()));
        return user;
    }
}
