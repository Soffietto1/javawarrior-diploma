package ru.itis.javawarrior.v2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.javawarrior.v2.enums.MethodEnum;
import ru.itis.javawarrior.v2.model.Method;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public interface MethodRepository extends JpaRepository<Method, MethodEnum> {
}
