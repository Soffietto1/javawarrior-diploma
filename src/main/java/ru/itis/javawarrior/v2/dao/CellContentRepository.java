package ru.itis.javawarrior.v2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.javawarrior.v2.model.CellContent;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public interface CellContentRepository extends JpaRepository<CellContent, Long> {
}
