package ru.itis.javawarrior.v2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.javawarrior.v2.model.User;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findOneByEmail(String email);
}
