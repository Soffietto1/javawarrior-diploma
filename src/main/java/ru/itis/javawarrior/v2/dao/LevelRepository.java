package ru.itis.javawarrior.v2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.javawarrior.v2.model.Level;

import java.util.List;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public interface LevelRepository extends JpaRepository<Level, Long> {

    List<Level> findAllByCreatorId(Long creatorId);
}
