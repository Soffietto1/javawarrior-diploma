package ru.itis.javawarrior.v2.json;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompileJson {
    private Long levelId;
    private String inputtedCode;
}
