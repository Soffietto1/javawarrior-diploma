package ru.itis.javawarrior.v2.json;

import lombok.Getter;
import lombok.Setter;
import ru.itis.javawarrior.v2.dto.CellContentDTO;
import ru.itis.javawarrior.v2.enums.MethodEnum;
import ru.itis.javawarrior.v2.model.CellContent;

import java.util.List;

/**
 * JSON параметров уровня, который нужно построить
 *
 * @author Azat Khayrullin
 * @since 05.05.2019
 */
@Getter
@Setter
public class LevelJson {
    private String backgroundImage;
    private List<CellContentDTO> contents;
    private List<MethodEnum> methods;
}
