package ru.itis.javawarrior.v2.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Damir Ilyasov
 */
@Setter
@Getter
@AllArgsConstructor
public class GameResult {
    private String message;
    private List<Action> actions;
    private boolean isStageCompleted;
    private String errorDescription;

    public static GameResult error(String error) {
        return new GameResult("", new ArrayList<>(), false, error);
    }

    public static GameResult completed(String message, List<Action> actions) {
        return new GameResult(message, actions, true, "");
    }

    public static GameResult notCompleted(String message, List<Action> actions) {
        return new GameResult(message, actions, false, "");
    }
}
