package ru.itis.javawarrior.v2.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ru.itis.javawarrior.v2.enums.Animation;

@Getter
@Setter
@AllArgsConstructor
public class Action {
    private int heroHp;
    private Animation animation;
    private int damaged;
}
