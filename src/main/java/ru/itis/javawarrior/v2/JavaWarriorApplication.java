package ru.itis.javawarrior.v2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itis.javawarrior.v2.enums.MethodEnum;
import ru.itis.javawarrior.v2.model.Level;
import ru.itis.javawarrior.v2.model.Method;
import ru.itis.javawarrior.v2.model.User;
import ru.itis.javawarrior.v2.service.LevelService;
import ru.itis.javawarrior.v2.service.UserService;
import ru.itis.javawarrior.v2.util.LevelUtil;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class JavaWarriorApplication  {

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    @Autowired
    private LevelService levelService;
    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(JavaWarriorApplication.class, args);
    }

    @PostConstruct
    private void init() {
        User admin = initAdmin();
        initLevel(admin);
    }

    private User initAdmin() {
        User admin = new User();
        admin.setName("Азат Пишу-божественный-код Хайруллин");
        admin.setEmail("admin");
        admin.setPassword(ENCODER.encode("admin"));
        userService.save(admin);
        return admin;
    }

    private void initLevel(User user) {
        Level level = LevelUtil.newLevel(LevelUtil.thirdLevel());
        List<Method> availableMethods = Arrays.asList(
                new Method(MethodEnum.WALK, "walk()", "Ходить"),
                new Method(MethodEnum.JUMP, "jump()", "Пригать"),
                new Method(MethodEnum.REST, "rest()", "Отдыхать"),
                new Method(MethodEnum.ATTACK, "attack()", "Атаковать"),
                new Method(MethodEnum.HEALTH, "health()", "Жизни"),
                new Method(MethodEnum.IS_ENEMY_AHEAD, "isEnemyAhead()", "Проверяет есть ли враг впереди")
        );
        level.setAvailableMethods(availableMethods);
        level.setCreator(user);
        levelService.save(level);
    }
}
