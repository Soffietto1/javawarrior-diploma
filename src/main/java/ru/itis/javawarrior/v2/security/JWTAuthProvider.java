package ru.itis.javawarrior.v2.security;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.itis.javawarrior.v2.model.User;
import ru.itis.javawarrior.v2.service.UserService;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
@Component
public class JWTAuthProvider implements AuthenticationProvider {

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    private UserService userService;
    private JWSVerifier verifier;

    public JWTAuthProvider(UserService userService) {
        this.verifier = new MACVerifier(
                "jfodfusdhfia" +
                        "ushfiqwuehfqwuhfsdhflakhe" +
                        "wqoifhkadlkahfiqwuehlchus" +
                        "hfiuwhuw" +
                        "asddddddddddddddddddddddd" +
                        "asdasdasdasdasdasdasdadas" +
                        "asdasdasdasdasdqwdqdqdqsd" +
                        "qsdqsdqsdqsdqdqsdqsdqdqsd");
        this.userService = userService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        JWTToken jwtToken = (JWTToken) authentication;
        JWT jwt = jwtToken.getJwt();

        // Check type of the parsed JOSE object
        if (jwt instanceof PlainJWT) {
            jwtToken.setAuthenticated(false);
            return jwtToken;
        } else if (jwt instanceof SignedJWT) {
            try {
                if (!((SignedJWT) jwt).verify(verifier)) {
                    jwtToken.setAuthenticated(false);
                    return jwtToken;
                }
            } catch (JOSEException e) {
                jwtToken.setAuthenticated(false);
                return jwtToken;
            }
        } else if (jwt instanceof EncryptedJWT) {
            jwtToken.setAuthenticated(false);
            return jwtToken;
        }

        ReadOnlyJWTClaimsSet claims = jwtToken.getClaims();
        String email = (String) claims.getClaim("email");
        String password = (String) claims.getClaim("password");
        User user = userService.findByEmail(email);
        if (user != null && (ENCODER.matches(password, user.getPassword()))) {
            jwtToken.setAuthenticated(true);
            return jwtToken;
        }
        jwtToken.setAuthenticated(false);
        return jwtToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JWTToken.class.isAssignableFrom(authentication);
    }

}
