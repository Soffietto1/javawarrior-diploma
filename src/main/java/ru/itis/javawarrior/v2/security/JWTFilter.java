package ru.itis.javawarrior.v2.security;

import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;

/**
 * TODO Class Description
 *
 * @author Azat Khayrullin
 */
public class JWTFilter extends GenericFilterBean {

    private JWTAuthProvider authenticationManager;

    @Autowired
    public JWTFilter(JWTAuthProvider authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public void afterPropertiesSet() {
        Assert.notNull(authenticationManager);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        try {
            String stringToken = req.getHeader("Authorization");
            if (stringToken == null) {
                res.sendError(HttpServletResponse.SC_OK, "Authorization header not found");
//                ((HttpServletResponse) response).sendRedirect("/v1/authorizationHeaderNotFound");

                throw new InsufficientAuthenticationException("Authorization header not found");
            }
            try {
                JWT jwt = JWTParser.parse(stringToken);
                JWTToken token = new JWTToken(jwt);
                Authentication auth = authenticationManager.authenticate(token);
                if (!auth.isAuthenticated()) {
                    res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Wrong token");
//                    ((HttpServletResponse) response).setStatus(2);
//                    ((HttpServletResponse) response).sendRedirect("/v1/wrong_token");
//                    ((HttpServletResponse) response).sendError(HttpServletResponse.SC_OK, "Wrong token");
                } else {
                    SecurityContextHolder.getContext().setAuthentication(auth);
                    chain.doFilter(request, response);
                }
            } catch (ParseException e) {
//                ((HttpServletResponse) response).sendRedirect("/v1/wrong_token");

                res.sendError(HttpServletResponse.SC_OK, "Wrong token");
            }
        } catch (AuthenticationException e) {
            SecurityContextHolder.clearContext();
        }
    }
}
